# Gauginos
Author Andrea Dell'Acqua (dellacqu@mail.cern.ch)
Converted from packagedoc.h

## Introduction

This package can be used to add Gauginos to the Geant4 simulation.  

## Class Overview

The package includes three classes:

 - G4Gravitino : The definition of the gravitino (useful for GMSB physics)
 - G4Neutralino : Useful for r-parity conserving mSUGRA models
 - GMSBDecays : Defines both the Neutralino and Gravitino for late decay of the neutralino inside the detector (GMSB studies)
