# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

from D3PDMakerCoreComps.D3PDObject import D3PDObject, make_SG_D3PDObject
from AthenaConfiguration.ComponentFactory   import CompFactory

D3PD = CompFactory.D3PD


AllSCD3PDObject = make_SG_D3PDObject( "CaloCellContainer",
                                   "AllCalo",
                                   "sc_",
                                   "SCD3PDObject" )
AllSCD3PDObject.defineBlock( 0, 'Basic', D3PD.SCFillerTool )


def _hookForSCD3PDObject_(c, flags, acc, *args, **kw):

    basFiller = c.BlockFillers[0]
    print("getattr(c, c.name()) / Type= ", type(basFiller))
    if "CaloEtaCut" in list(kw.keys()):
        basFiller.CaloEtaCut = kw["CaloEtaCut"]
    if "CaloPhiCut" in list(kw.keys()):
        basFiller.CaloPhiCut = kw["CaloPhiCut"]
    if "CaloLayers" in list(kw.keys()):
        basFiller.CaloLayers = kw["CaloLayers"]
    if "CaloDetectors" in list(kw.keys()):
        basFiller.CaloDetectors = kw["CaloDetectors"]
    if "TileDLayerOption" in list(kw.keys()):
        basFiller.TileDLayerOption = kw["TileDLayerOption"]
        
    print("%s - CaloEtaCut = " % (basFiller.name), basFiller.CaloEtaCut)
    print("%s - CaloPhiCut = " % (basFiller.name), basFiller.CaloPhiCut)
    print("%s - CaloLayersCut = " % (basFiller.name), basFiller.CaloLayers)
    print("%s - CaloDetectors = " % (basFiller.name), basFiller.CaloDetectors)
    #print "%s - TileDLayerOption = " % (basFiller.name), basFiller.TileDLayerOption

    from LArCabling.LArCablingConfig import LArOnOffIdMappingCfg, LArOnOffIdMappingSCCfg
    acc.merge (LArOnOffIdMappingCfg (flags))
    acc.merge (LArOnOffIdMappingSCCfg (flags))
    return 

def _makeSC_obj_(name, prefix, object_name,
                       getter=None,
                       sgKey=None,
                       typeName=None,
                       ):
    from D3PDMakerConfig.D3PDMakerFlags import D3PDMakerFlags
    if not typeName:
        typeName = "CaloCellContainer"
    if not sgKey:
        sgKey="AllCalo"
    if not getter:
        getter = D3PD.SGObjGetterTool(
            name + '_Getter',
            TypeName = typeName,
            SGKey = sgKey)

    return D3PD.ObjFillerTool( name,
                               Prefix = prefix,
                               Getter = getter,
                               ObjectName = object_name,
                               SaveMetadata = \
                               D3PDMakerFlags.SaveObjectMetadata
                              )
    
def make_SCD3PDObject( typeName="CaloCellContainer",
                             sgKey="AllCalo",
                             prefix="sc_",
                             object_name="SCD3PDObject",
                             *args, **kw ):
        
    obj = D3PDObject(_makeSC_obj_, prefix, object_name,
                     allow_args=["CaloEtaCut","CaloPhiCut",
                                 "CaloLayers","CaloDetectors","TileDLayerOption"],
                     sgkey=sgKey,
                     typename=typeName)
    obj.defineBlock( 0, 'Basic', D3PD.SCFillerTool)
    obj.defineHook( _hookForSCD3PDObject_ )
    return obj

SCD3PDObject = make_SCD3PDObject( typeName="CaloCellContainer",
                                   sgKey = "AllCalo",
                                   prefix = "sc_",
                                   CaloEtaCut=[],
                                   CaloPhiCut=[],
                                   CaloLayers=[],
                                   CaloDetectors=[],
                                   TileDLayerOption=False )
