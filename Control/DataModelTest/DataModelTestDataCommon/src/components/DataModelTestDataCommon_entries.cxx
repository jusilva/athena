/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file DataModelTestDataCommon/src/components/DataModelTestDataCommon_entries.cxx
 * @author snyder@bnl.gov
 * @date Apr, 2017
 * @brief Gaudi algorithm factory declarations.
 */


#include "../xAODTestReadSymlink.h"
#include "../xAODTestReadDecor.h"
#include "../xAODTestDecor.h"
#include "../xAODTestAlg.h"
#include "../xAODTestWriteCVec.h"
#include "../xAODTestWriteCInfo.h"
#include "../xAODTestThinCVec.h"
#include "../xAODTestWritePVec.h"
#include "../xAODTestReadPVec.h"
#include "../xAODTestThinJVec.h"
#include "../xAODTestThinPLinks.h"
#include "../xAODTestWriteCLinks.h"
#include "../xAODTestWriteFwdLink1.h"
#include "../xAODTestWriteFwdLink2.h"
#include "../xAODTestReadCVec.h"
#include "../xAODTestReadCLinks.h"
#include "../xAODTestShallowCopy.h"
#include "../xAODTestWriteJVec.h"
#include "../xAODTestReadJVec.h"
#include "../xAODTestWritePLinks.h"
#include "../xAODTestReadPLinks.h"
#include "../CondWriterAlg.h"
#include "../CondWriterExtAlg.h"
#include "../CondReaderAlg.h"
#include "../xAODTestReadSymlinkTool.h"
#include "../CondAlg1.h"
#include "../CondAlg2.h"
#include "../MetaWriterAlg.h"
#include "../MetaReaderAlg.h"
#include "../DummyDecisionWriter.h"

DECLARE_COMPONENT( DMTest::xAODTestReadSymlink )
DECLARE_COMPONENT( DMTest::xAODTestReadDecor )
DECLARE_COMPONENT( DMTest::xAODTestDecor )
DECLARE_COMPONENT( DMTest::xAODTestAlg )
DECLARE_COMPONENT( DMTest::xAODTestWriteCVec )
DECLARE_COMPONENT( DMTest::xAODTestWriteCInfo )
DECLARE_COMPONENT( DMTest::xAODTestThinCVec )
DECLARE_COMPONENT( DMTest::xAODTestWritePVec )
DECLARE_COMPONENT( DMTest::xAODTestReadPVec )
DECLARE_COMPONENT( DMTest::xAODTestThinJVec )
DECLARE_COMPONENT( DMTest::xAODTestThinPLinks )
DECLARE_COMPONENT( DMTest::xAODTestWriteCLinks )
DECLARE_COMPONENT( DMTest::xAODTestWriteFwdLink1 )
DECLARE_COMPONENT( DMTest::xAODTestWriteFwdLink2 )
DECLARE_COMPONENT( DMTest::xAODTestReadCVec )
DECLARE_COMPONENT( DMTest::xAODTestReadCLinks )
DECLARE_COMPONENT( DMTest::xAODTestShallowCopy )
DECLARE_COMPONENT( DMTest::xAODTestWriteJVec )
DECLARE_COMPONENT( DMTest::xAODTestReadJVec )
DECLARE_COMPONENT( DMTest::xAODTestWritePLinks )
DECLARE_COMPONENT( DMTest::xAODTestReadPLinks )
DECLARE_COMPONENT( DMTest::CondWriterAlg )
DECLARE_COMPONENT( DMTest::CondWriterExtAlg )
DECLARE_COMPONENT( DMTest::CondReaderAlg )
DECLARE_COMPONENT( DMTest::CondAlg1 )
DECLARE_COMPONENT( DMTest::CondAlg2 )
DECLARE_COMPONENT( DMTest::MetaWriterAlg )
DECLARE_COMPONENT( DMTest::MetaReaderAlg )
DECLARE_COMPONENT( DMTest::DummyDecisionWriter )

DECLARE_COMPONENT( DMTest::xAODTestReadSymlinkTool )

