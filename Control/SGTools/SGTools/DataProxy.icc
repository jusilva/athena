// This file's extension implies that it's C, but it's really -*- C++ -*-.

/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

/***************************************************************************
 implementation of DataProxy_cast operator
 -----------------------------------------
 ATLAS Collaboration
***************************************************************************/

// $Id: DataProxy.icc,v 1.6 2008-07-14 22:16:25 calaf Exp $

#include "AthenaKernel/DataBucketBase.h"
#include "AthenaKernel/StorableConversions.h"
#include "AthenaKernel/ClassID_traits.h"
#ifndef NDEBUG
#  include "AthenaKernel/getMessageSvc.h"
#  include "GaudiKernel/MsgStream.h"
#endif


class DataObject;


/// Retrieve data object key == string
inline
const SG::DataProxy::name_type& SG::DataProxy::name() const
{
  // No locking needed.
  return m_tAddress.name();
}


/// Retrieve data object key == string
/// duplicated for Gaudi folks does same as name()
inline
const SG::DataProxy::id_type& SG::DataProxy::identifier() const
{
  // No locking needed.
  return m_tAddress.name();
}


/// Retrieve DataObject
inline
DataObject* SG::DataProxy::object ATLAS_NOT_CONST_THREAD_SAFE () const
{
  return m_dObject;
}


/// Retrieve IOpaqueAddress
inline
IOpaqueAddress* SG::DataProxy::address() const
{
  lock_t lock (m_mutex);
  return m_tAddress.address();
} 


/// set DataSvc (Gaudi-specific); do nothing for us
inline
IDataProviderSvc* SG::DataProxy::dataSvc() const
{
  return nullptr;
}

///< Get the primary (hashed) SG key.
inline
SG::sgkey_t SG::DataProxy::sgkey() const
{
  // No lock; underlying member is atomic.
  return m_tAddress.sgkey();
}


/// Set the primary (hashed) SG key.
inline
void SG::DataProxy::setSGKey (sgkey_t sgkey)
{
  lock_t lock (m_mutex);
  m_tAddress.setSGKey (sgkey);
}


///< Return the ID of the store containing this proxy.
inline
StoreID::type SG::DataProxy::storeID() const
{
  lock_t lock (m_mutex);
  return m_tAddress.storeID();
}


///< check if it is a transient ID (primary or symLinked):
inline
bool SG::DataProxy::transientID (CLID id) const
{
  lock_t lock (m_mutex);
  return m_tAddress.transientID (id);
}


///< return the list of transient IDs (primary or symLinked):
inline
SG::DataProxy::CLIDCont_t SG::DataProxy::transientID() const
{
  lock_t lock (m_mutex);
  return m_tAddress.transientID();
}


/// Add a new transient ID
inline
void SG::DataProxy::setTransientID(CLID id)
{
  lock_t lock (m_mutex);
  m_tAddress.setTransientID (id);
}


/// access set of proxy aliases
/// Returns a COPY of the alias set.
inline
SG::DataProxy::AliasCont_t SG::DataProxy::alias() const
{
  lock_t lock (m_mutex);
  return m_tAddress.alias();
}


/// Add a new proxy alias.
inline
void SG::DataProxy::setAlias(const std::string& key)
{
  lock_t lock (m_mutex);
  m_tAddress.setAlias (key);
}


/// Test to see if a given string is in the alias set.
inline
bool SG::DataProxy::hasAlias(const std::string& key) const
{
  lock_t lock (m_mutex);
  const AliasCont_t& alias = m_tAddress.alias();
  return alias.find(key) != alias.end();
}


/// remove alias from proxy
inline
bool SG::DataProxy::removeAlias(const std::string& key)
{
  lock_t lock (m_mutex);
  return m_tAddress.removeAlias (key);
}


/// Return the address provider.
inline
IAddressProvider* SG::DataProxy::provider()
{
  lock_t lock (m_mutex);
  return m_tAddress.provider();
}


/// Set the address provider.
inline
void SG::DataProxy::setProvider(IAddressProvider* provider,
                                StoreID::type storeID)
{
  lock_t lock (m_mutex);
  m_tAddress.setProvider (provider, storeID);
}


/// Set the CLID / key.
/// This will only succeed if the clid/key are currently clear.
inline
void SG::DataProxy::setID (CLID id, const std::string& key)
{
  lock_t lock (m_mutex);
  m_tAddress.setID (id, key);
}


/// am I valid?
inline
bool SG::DataProxy::isValid() const
{
  return (isValidObject() || isValidAddress());
}


/// is the object valid?
inline
bool SG::DataProxy::isValidObject() const
{
  // FIXME: should we try to chase?
  return (0!= m_dObject);
}


/// Access DataObject on-demand using conversion service
///@throws runtime_error when converter fails
inline
DataObject* SG::DataProxy::accessData()
{
  // Inlined part: return m_dObject if it's valid.
  // Otherwise, call the out-of-line part of the code.
  if (0 != m_dObject) return m_dObject;  // cached object
  return accessDataOol();
}


inline
SG::DataProxy::ErrNo SG::DataProxy::errNo() const
{
  lock_t lock (m_objMutex);
  return m_errno;
}


/// Retrieve clid
inline
CLID SG::DataProxy::clID() const
{
  // No lock; underlying member is atomic.
  return m_tAddress.clID();
}


/// Check if it is a const object
inline
bool SG::DataProxy::isConst() const
{
  // No lock; underlying member is atomic.
  return m_const;
}


/// set the reset only flag: Clear Store will reset and not delete.
inline
void SG::DataProxy::resetOnly(const bool& flag)
{
  lock_t lock (m_mutex);
  m_resetFlag = flag;
}


/// Check reset only:
inline
bool SG::DataProxy::isResetOnly() const
{
  lock_t lock (m_mutex);
  return m_resetFlag;
}


/// Set the store of which we're a part.
inline
void SG::DataProxy::setStore (IProxyDict* store)
{
  m_store = store;
}

/// Return the store of which we're a part.
inline
IProxyDict* SG::DataProxy::store()
{
  return m_store;
}


/// Return the store of which we're a part.
inline
const IProxyDict* SG::DataProxy::store() const
{
  return m_store;
}


inline
IConverter* SG::DataProxy::loader()
{
  lock_t lock (m_mutex);
  return m_dataLoader;
}



